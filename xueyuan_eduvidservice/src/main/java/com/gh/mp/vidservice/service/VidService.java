package com.gh.mp.vidservice.service;

import com.aliyuncs.exceptions.ClientException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface VidService {
    String uploadVideoAliyun(MultipartFile file) throws IOException;

    void deleteAliyunVideo(String videoId) throws ClientException;

    void deleteBatch(List<String> videoIdList);

    String getPlayAuth(String id) throws ClientException;
}
