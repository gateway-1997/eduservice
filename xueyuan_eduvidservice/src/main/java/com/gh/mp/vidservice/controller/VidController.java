package com.gh.mp.vidservice.controller;

import com.aliyuncs.exceptions.ClientException;
import com.gh.mp.edu.common.R;
import com.gh.mp.vidservice.service.VidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @Author Eric
 * @Date 2021/4/29 22:36
 * @Version 1.0
 */
@RestController
@RequestMapping("/vidservice/vod")
@CrossOrigin
public class VidController {

    @Autowired
    private VidService vidService;
    /**
     * 上传视频到阿里云服务器
     * @return
     */
    @PostMapping("upload")
    public R uploadAliyunVideo(@RequestParam("file") MultipartFile file) throws IOException {
        String videoId=vidService.uploadVideoAliyun(file);
        return R.ok().data("videoId",videoId);
    }
    @DeleteMapping("{videoId}")
    public R deleteAliyunVideo(@PathVariable String videoId) throws ClientException {
        vidService.deleteAliyunVideo(videoId);
        return R.ok();
    }
    @DeleteMapping("delete-batch")
    public R deleteBatch(@RequestParam("videoIdList") List<String> videoIdList){
        vidService.deleteBatch(videoIdList);
        return R.ok();
    }
    /**
     * 根据视频id获取视频播放凭证
     */
    @GetMapping("getPlayAuth/{id}")
    public R getPlayAuth(@PathVariable String id) throws ClientException {
       String auth= vidService.getPlayAuth(id);
       return R.ok().data("playAuth",auth);
    }
}
