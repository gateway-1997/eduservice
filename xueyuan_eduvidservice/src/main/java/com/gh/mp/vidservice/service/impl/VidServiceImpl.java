package com.gh.mp.vidservice.service.impl;

import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadStreamRequest;
import com.aliyun.vod.upload.req.UploadVideoRequest;
import com.aliyun.vod.upload.resp.UploadStreamResponse;
import com.aliyun.vod.upload.resp.UploadVideoResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.DeleteVideoRequest;
import com.aliyuncs.vod.model.v20170321.DeleteVideoResponse;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;
import com.gh.mp.vidservice.service.VidService;
import com.gh.mp.vidservice.utils.ConstantPropertiesUtil;
import com.gh.mp.vidservice.utils.InitObjectUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @Author Eric
 * @Date 2021/4/29 22:40
 * @Version 1.0
 */
@Service
public class VidServiceImpl implements VidService {
    @Override
    public String uploadVideoAliyun(MultipartFile file) throws IOException {
        String accessKeyId = ConstantPropertiesUtil.KEYID;
        String accessKeySecret = ConstantPropertiesUtil.KEYSECRET;
        String fileName = file.getOriginalFilename();
        String title = fileName.substring(0, fileName.lastIndexOf("."));
        InputStream inputStream = file.getInputStream();
        UploadStreamRequest request = new UploadStreamRequest(accessKeyId, accessKeySecret, title, fileName, inputStream);
        UploadVideoImpl uploader = new UploadVideoImpl();
        UploadStreamResponse response = uploader.uploadStream(request);
        String videoId = response.getVideoId();
        return videoId;
    }

    @Override
    public void deleteAliyunVideo(String videoId) throws ClientException {
        DefaultAcsClient client = InitObjectUtil.initVodClient(ConstantPropertiesUtil.KEYID, ConstantPropertiesUtil.KEYSECRET);
        DeleteVideoRequest request = new DeleteVideoRequest();
        request.setVideoIds(videoId);
        DeleteVideoResponse response = client.getAcsResponse(request);
    }

    @Override
    public void deleteBatch(List<String> videoIdList) {
        try {
            DefaultAcsClient client = InitObjectUtil.initVodClient(ConstantPropertiesUtil.KEYID, ConstantPropertiesUtil.KEYSECRET);
            DeleteVideoRequest request = new DeleteVideoRequest();
            String videoIds = StringUtils.join(videoIdList.toArray(), ",");
            request.setVideoIds(videoIds);
            DeleteVideoResponse response = client.getAcsResponse(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public String getPlayAuth(String id) throws ClientException {
        DefaultAcsClient client = InitObjectUtil.initVodClient(ConstantPropertiesUtil.KEYID, ConstantPropertiesUtil.KEYSECRET);
        GetVideoPlayAuthRequest request=new GetVideoPlayAuthRequest();
        request.setVideoId(id);
        GetVideoPlayAuthResponse response = client.getAcsResponse(request);
        String playAuth = response.getPlayAuth();
        return playAuth;
    }
}
