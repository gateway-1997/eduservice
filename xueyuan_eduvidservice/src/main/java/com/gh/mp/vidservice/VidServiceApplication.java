package com.gh.mp.vidservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Author Eric
 * @Date 2021/4/29 21:40
 * @Version 1.0
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class VidServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(VidServiceApplication.class,args);
    }

}