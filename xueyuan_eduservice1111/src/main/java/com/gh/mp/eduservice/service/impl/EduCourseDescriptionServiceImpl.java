package com.gh.mp.eduservice.service.impl;

import com.gh.mp.eduservice.entity.EduCourseDescription;
import com.gh.mp.eduservice.mapper.EduCourseDescriptionMapper;
import com.gh.mp.eduservice.service.EduCourseDescriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程简介 服务实现类
 * </p>
 *
 * @author Eric
 * @since 2021-04-24
 */
@Service
public class EduCourseDescriptionServiceImpl extends ServiceImpl<EduCourseDescriptionMapper, EduCourseDescription> implements EduCourseDescriptionService {

}
