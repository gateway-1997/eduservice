package com.gh.mp.eduservice.entity.dto;

import lombok.Data;

import java.util.List;

/**
 * @Author Eric
 * @Date 2021/4/25 14:47
 * @Version 1.0
 */
@Data
public class EduChapterDto {

    private String id;

    private String title;

    private List<EduVideoDto> children;
}
