package com.gh.mp.eduservice.controller.front;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gh.mp.edu.common.R;
import com.gh.mp.eduservice.entity.EduCourse;
import com.gh.mp.eduservice.entity.EduTeacher;
import com.gh.mp.eduservice.service.EduCourseService;
import com.gh.mp.eduservice.service.EduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author Eric
 * @Date 2021/5/11 22:27
 * @Version 1.0
 */
@RestController
@RequestMapping("/eduservice/index-front")
@CrossOrigin
public class IndexFrontController {
    @Autowired
    private EduCourseService courseService;

    @Autowired
    private EduTeacherService teacherService;

    //查询前八条热门课程,查询前四名师
    @GetMapping("index")
    public R findTeacherAndCourse(){
        QueryWrapper<EduCourse> courseQueryWrapper=new QueryWrapper<>();
        courseQueryWrapper.orderByDesc("id");
        courseQueryWrapper.last("limit 8");
        QueryWrapper<EduTeacher> teacherQueryWrapper=new QueryWrapper<>();
        teacherQueryWrapper.orderByDesc("id");
        teacherQueryWrapper.last("limit 4");
        List<EduCourse> eduCourses = courseService.list(courseQueryWrapper);
        List<EduTeacher> eduTeachers = teacherService.list(teacherQueryWrapper);
        return R.ok().data("teacherList",eduTeachers).data("courseList",eduCourses);
    }

}
