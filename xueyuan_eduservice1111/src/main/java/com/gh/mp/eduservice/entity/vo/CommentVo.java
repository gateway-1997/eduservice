package com.gh.mp.eduservice.entity.vo;

import lombok.Data;

/**
 * @Author Eric
 * @Date 2021/5/19 23:48
 * @Version 1.0
 */
@Data
public class CommentVo {
    private String courseId;
    private String teacherId;
    private String content;

    private String memberId;
}
