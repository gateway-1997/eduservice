package com.gh.mp.eduservice.controller.front;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gh.mp.edu.common.R;
import com.gh.mp.edu.exception.EduException;
import com.gh.mp.edu.utils.JwtUtils;
import com.gh.mp.eduservice.client.OrderClient;
import com.gh.mp.eduservice.entity.EduCourse;
import com.gh.mp.eduservice.entity.dto.EduChapterDto;
import com.gh.mp.eduservice.entity.frontVo.CourseFrontVo;
import com.gh.mp.eduservice.entity.frontVo.CourseInfoFrontVo;
import com.gh.mp.eduservice.service.EduChapterService;
import com.gh.mp.eduservice.service.EduCourseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @Author Eric
 * @Date 2021/5/16 20:28
 * @Version 1.0
 */
@RestController
@RequestMapping("/eduservice/coursefront")
@CrossOrigin
public class CourseFrontController {

    @Autowired
    private EduCourseService eduCourseService;

    @Autowired
    private EduChapterService eduChapterService;

    @Autowired
    private OrderClient ordersClient;

    @PostMapping("getFrontCourseList/{page}/{limit}")
    public R getFrontCourseList(@PathVariable Long page,
                                @PathVariable Long limit,
                                @RequestBody(required = false) CourseFrontVo courseFrontVo){
        Page<EduCourse> pageCourse=new Page<>(page,limit);
        Map<String,Object> map = eduCourseService.getFrontCourseList(pageCourse,courseFrontVo);
        return R.ok().data(map);
    }

    @PostMapping("getCourseInfoById/{courseId}")
    public R getFrontCourseInfo(@PathVariable String courseId, HttpServletRequest request){
        //根据课程id和用户id查询当前课程是否已经支付过了
        String token = JwtUtils.getMemberIdByJwtToken(request);

        CourseInfoFrontVo courseInfoFrontVo= eduCourseService.getFrontCourseInfo(courseId);
        //根据课程id查询章节和小节
        List<EduChapterDto> chapterVideoList = eduChapterService.getChapterVideoList(courseId);

        boolean buyCourse = ordersClient.isBuyCourse(courseId,token);

        return R.ok().data("courseInfoFrontVo",courseInfoFrontVo).data("chapterVideoList",chapterVideoList).data("isBuy",buyCourse);
    }
    //根据课程id查询课程信息
    @PostMapping("getCourseOrderById/{id}")
    public CourseInfoFrontVo getFrontCourseOrder(@PathVariable String id){
        CourseInfoFrontVo courseInfoFrontVo= eduCourseService.getFrontCourseInfo(id);
        return courseInfoFrontVo;
    }


}
