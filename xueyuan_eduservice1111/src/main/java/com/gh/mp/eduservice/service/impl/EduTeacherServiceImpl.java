package com.gh.mp.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gh.mp.eduservice.entity.EduTeacher;
import com.gh.mp.eduservice.entity.query.QueryTeacher;
import com.gh.mp.eduservice.mapper.EduTeacherMapper;
import com.gh.mp.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Eric
 * @since 2021-03-31
 */
@Service
public class EduTeacherServiceImpl extends ServiceImpl<EduTeacherMapper, EduTeacher> implements EduTeacherService {

    @Override
    public void pageListCondition(Page<EduTeacher> pageTeacher, QueryTeacher queryTeacher) {
        //关键:queryTeacher中有传递过来的条件值，判断，如果有条件就拼接，没有就不。
        if (queryTeacher == null) {
            //直接查询分页，不进行条件查询
            baseMapper.selectPage(pageTeacher, null);
            return;
        }
        //如果queryTeacher不为空
        String name = queryTeacher.getName();
        String level = queryTeacher.getLevel();
        String startTime = queryTeacher.getStartTime();
        String endTime = queryTeacher.getEndTime();
        //判断条件是否有，如果有就拼接条件
        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(name)) {
            wrapper.like("name", name);
        }
        if (!StringUtils.isEmpty(level)) {
            wrapper.eq("level", level);
        }
        if (!StringUtils.isEmpty(startTime)) {
            wrapper.ge("gmt_create", startTime);
        }
        if (!StringUtils.isEmpty(endTime)) {
            wrapper.le("gmt_create", endTime);
        }
        baseMapper.selectPage(pageTeacher, wrapper);
    }

    @Override
    public Map<String, Object> getTeacherFrontList(Page<EduTeacher> pageTeacher) {
        QueryWrapper<EduTeacher> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("id");
        baseMapper.selectPage(pageTeacher, queryWrapper);
        List<EduTeacher> records = pageTeacher.getRecords();
        long total = pageTeacher.getTotal();
        long pages = pageTeacher.getPages();
        long size = pageTeacher.getSize();
        long current = pageTeacher.getCurrent();
        boolean hasNext = pageTeacher.hasNext();
        boolean hasPrevious = pageTeacher.hasPrevious();
        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("records", records);
        map.put("pages", pages);
        map.put("size", size);
        map.put("current", current);
        map.put("hasNext", hasNext);
        map.put("hasPrevious", hasPrevious);
        return map;
    }
}
