package com.gh.mp.eduservice.entity.vo;

import lombok.Data;

/**
 * @Author Eric
 * @Date 2021/4/21 21:02
 * @Version 1.0
 */
@Data
public class SubjectVo {

    private String id;

    private String title;
}
