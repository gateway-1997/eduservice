package com.gh.mp.eduservice.entity.dto;

import lombok.Data;

/**
 * @Author Eric
 * @Date 2021/4/25 14:49
 * @Version 1.0
 */
@Data
public class EduVideoDto {

    private String id;

    private String title;

    private String videoSourceId;
}
