package com.gh.mp.eduservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Author Eric
 * @Date 2021/5/23 11:34
 * @Version 1.0
 */
@FeignClient(name = "service-order")
@Component
public interface OrderClient {

    //根据课程id和用户id查询订单表中订单状态
    @GetMapping("eduOrder/t-order/isBuyCourse/{courseId}/{memberId}")
    boolean isBuyCourse(@PathVariable("courseId") String courseId,
                        @PathVariable("memberId") String memberId);
}
