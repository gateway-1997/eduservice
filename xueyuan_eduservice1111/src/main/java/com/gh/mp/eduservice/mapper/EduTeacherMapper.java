package com.gh.mp.eduservice.mapper;

import com.gh.mp.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Eric
 * @since 2021-03-31
 */
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
