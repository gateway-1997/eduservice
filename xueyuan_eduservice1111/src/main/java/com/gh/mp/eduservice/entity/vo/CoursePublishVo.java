package com.gh.mp.eduservice.entity.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author Eric
 * @Date 2021/4/26 23:10
 * @Version 1.0
 */
@Data
public class CoursePublishVo {

    private String title;
    private Integer lessonNum;
    private String subjectLevelOne;
    private String subjectLevelTwo;
    private String teacherName;
    private BigDecimal price;
    private String cover;

}
