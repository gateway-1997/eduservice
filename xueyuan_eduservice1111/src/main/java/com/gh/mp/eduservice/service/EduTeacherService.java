package com.gh.mp.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gh.mp.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gh.mp.eduservice.entity.query.QueryTeacher;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Eric
 * @since 2021-03-31
 */
public interface EduTeacherService extends IService<EduTeacher> {

    void pageListCondition(Page<EduTeacher> pageTeacher, QueryTeacher queryTeacher);

    Map<String, Object> getTeacherFrontList(Page<EduTeacher> pageTeacher);
}
