package com.gh.mp.eduservice.controller;


import com.gh.mp.edu.common.R;
import com.gh.mp.eduservice.entity.EduSubject;
import com.gh.mp.eduservice.service.EduSubjectService;
import com.gh.mp.eduservice.entity.vo.SubjectNestedVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author Eric
 * @since 2021-04-19
 */
@RestController
@RequestMapping("/eduservice/edu-subject")
@CrossOrigin
public class EduSubjectController {

    @Autowired
    private EduSubjectService eduSubjectService;

    @PostMapping("import")
    public R importExcelSubject(@RequestParam("file") MultipartFile multipartFile) {

        List<String> list = eduSubjectService.importExcelSubject(multipartFile);
        if (list.size() == 0) {
            return R.ok();
        } else {
            return R.error().data("msgList", list);
        }
    }

    @GetMapping()
    public R getNestedTreeList() {
        List<SubjectNestedVo> subjectNestedVoList = eduSubjectService.nestedList();

        return R.ok().data("items", subjectNestedVoList);
    }

    @ApiOperation(value = "新增一级分类")
    @PostMapping("insertSubject")
    public R insertSubject( @ApiParam(name = "subject", value = "课程分类对象", required = true)
                               @RequestBody EduSubject subject) {
        boolean result = eduSubjectService.insertSubject(subject);
        if (result) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    @DeleteMapping("{id}")
    public R deleteSubjectId(@PathVariable String id) {
        boolean flag = eduSubjectService.deleteSubjectById(id);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }

}

