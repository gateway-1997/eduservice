package com.gh.mp.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gh.mp.eduservice.entity.EduChapter;
import com.gh.mp.eduservice.entity.EduVideo;
import com.gh.mp.eduservice.entity.dto.EduChapterDto;
import com.gh.mp.eduservice.entity.dto.EduVideoDto;
import com.gh.mp.edu.exception.EduException;
import com.gh.mp.eduservice.mapper.EduChapterMapper;
import com.gh.mp.eduservice.service.EduChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gh.mp.eduservice.service.EduVideoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author Eric
 * @since 2021-04-25
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {

    @Autowired
    private EduVideoService videoService;

    @Override
    public List<EduChapterDto> getChapterVideoList(String id) {
        List<EduChapterDto> list = new ArrayList<>();
        //根据课程id查询章节
        QueryWrapper<EduChapter> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_id", id);
        List<EduChapter> eduChapters = baseMapper.selectList(queryWrapper);
        //根据课程id查询小节
        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", id);
        List<EduVideo> eduVideos = videoService.list(wrapper);
        //遍历所有的章节，复制到dto对象里面
        eduChapters.forEach(chapter -> {
            List<EduVideoDto> videoDtoList = new ArrayList<>();//用来存小节
            EduChapterDto chapterDto = new EduChapterDto();
            BeanUtils.copyProperties(chapter, chapterDto);
            //遍历所有的小节，复制到dto对象里面
            eduVideos.forEach(video -> {

                if (video.getChapterId().equals(chapter.getId())) {
                    EduVideoDto eduVideoDto = new EduVideoDto();
                    BeanUtils.copyProperties(video, eduVideoDto);
                    videoDtoList.add(eduVideoDto);
                    chapterDto.setChildren(videoDtoList);
                }
            });

            list.add(chapterDto);
        });

        return list;
    }

    @Override
    public Boolean addChapter(EduChapter eduChapter) {
        int result = baseMapper.insert(eduChapter);
        return result > 0;
    }

    @Override
    public Boolean updateChapterById(EduChapter eduChapter) {
        int i = baseMapper.updateById(eduChapter);
        return i > 0;
    }

    @Override
    public EduChapter findChapterInfoById(String id) {
        EduChapter chapter = baseMapper.selectById(id);
        return chapter;
    }

    @Override
    public Boolean deleteChapter(String id) {
        QueryWrapper<EduVideo> wrapper = new QueryWrapper();
        wrapper.eq("chapter_id", id);
        Integer count = videoService.count(wrapper);
        //判断有没有小节，如果有就不能删
        if (count == 0) {
            int i = baseMapper.deleteById(id);
            return i > 0;
        }
        throw new EduException(20001, "章节下面有小节，请先删除小节");
    }
}
