package com.gh.mp.eduservice.mapper;

import com.gh.mp.eduservice.entity.EduComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论 Mapper 接口
 * </p>
 *
 * @author Eric
 * @since 2021-05-19
 */
public interface EduCommentMapper extends BaseMapper<EduComment> {

}
