package com.gh.mp.eduservice.mapper;

import com.gh.mp.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程简介 Mapper 接口
 * </p>
 *
 * @author Eric
 * @since 2021-04-24
 */
public interface EduCourseDescriptionMapper extends BaseMapper<EduCourseDescription> {

}
