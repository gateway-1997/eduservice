package com.gh.mp.eduservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gh.mp.edu.common.R;
import com.gh.mp.eduservice.entity.EduTeacher;
import com.gh.mp.eduservice.entity.query.QueryTeacher;
import com.gh.mp.eduservice.service.EduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Eric
 * @since 2021-03-31
 */
@RestController
@RequestMapping("/eduservice/edu-teacher")
@CrossOrigin
public class EduTeacherController {

    @Autowired
    private EduTeacherService service;

    //模拟登录
    @PostMapping("login")
    public R login(){
        return R.ok().data("token","admin");
    }
    @GetMapping("info")
    public R info(){
        return R.ok().data("roles","[admin]").data("name","admin").data("avatar","https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
    }

    @GetMapping()
    public R getAllTeacherList(){
        List<EduTeacher> list = service.list(null);
        return R.ok().data("item",list);
    }
    @DeleteMapping("{id}")
    public R deleteTeacherById(@PathVariable String id){
        //TODO 删除讲师头像
        service.removeById(id);
        return R.ok();
    }
    //分页查询讲师列表的方法
    @GetMapping("pageList/{page}/{limit}")
    public R getPageTeacherList(@PathVariable Long page,
                                @PathVariable Long limit){
        //创建page对象，创建参数
        Page<EduTeacher> pageTeacher=new Page<>(page,limit);
        service.page(pageTeacher,null);
        //从pageTeacher获取数据
        long total = pageTeacher.getTotal();//总条数
        List<EduTeacher> records = pageTeacher.getRecords();//总记录

        return R.ok().data("total",total).data("items",records);
    }

    @PostMapping("moreConditionPageList/{page}/{limit}")
    public R getMoreConditionPageList(@PathVariable Long page,
                                      @PathVariable Long limit,
                                      @RequestBody(required = false) QueryTeacher queryTeacher){
        Page<EduTeacher> pageTeacher = new Page<>(page,limit);
        //调用service的方法实现条件查询带分页
        service.pageListCondition(pageTeacher,queryTeacher);
        long total = pageTeacher.getTotal();//总条数
        List<EduTeacher> records = pageTeacher.getRecords();//总记录
        Map<String,Object> map=new HashMap<>();
        map.put("total",total);
        map.put("items",records);
        return R.ok().data(map);
    }
    @PostMapping("addTeacher")
    public R addTeacher(@RequestBody EduTeacher eduTeacher){
        eduTeacher.setIsDeleted(false);
        eduTeacher.setId(UUID.randomUUID().toString());
        boolean save = service.save(eduTeacher);
        if(save){
            return R.ok();
        }else{
            return R.error();
        }
    }
    //根据id查询讲师信息
    @GetMapping("getTeacherInfo/{id}")
    public R findTeacherById(@PathVariable String id){
        EduTeacher eduTeacher = service.getById(id);
        return R.ok().data("eduTeacher",eduTeacher);
    }
    //根据id修改讲师信息
    @PutMapping("updateTeacher")
    public R updateTeacher(@RequestBody EduTeacher eduTeacher){
        boolean b = service.updateById(eduTeacher);
        if(b){
            return R.ok();
        }
        return R.error();
    }
}

