package com.gh.mp.eduservice.entity.query;

import lombok.Data;

/**
 * @Author Eric
 * @Date 2021/4/25 10:27
 * @Version 1.0
 */
@Data
public class QueryCourse {

    private String subjectParentId;
    private String subjectId;
    private String title;
    private String teacherId;
}
