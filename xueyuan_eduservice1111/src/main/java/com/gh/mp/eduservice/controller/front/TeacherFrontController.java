package com.gh.mp.eduservice.controller.front;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gh.mp.edu.common.R;
import com.gh.mp.eduservice.entity.EduCourse;
import com.gh.mp.eduservice.entity.EduTeacher;
import com.gh.mp.eduservice.service.EduCourseService;
import com.gh.mp.eduservice.service.EduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Author Eric
 * @Date 2021/5/16 20:18
 * @Version 1.0
 */
@RestController
@CrossOrigin
@RequestMapping("/eduservice/index-front")
public class TeacherFrontController {
    @Autowired
    private EduTeacherService teacherService;

    @Autowired
    private EduCourseService courseService;

    /**
     * 分页查询所有讲师
     * @param page
     * @param limit
     * @return
     */
    @PostMapping("getFrontTeacherList/{page}/{limit}")
    public R getTeacherList(@PathVariable Long page,
                            @PathVariable Long limit){
        Page<EduTeacher> pageTeacher=new Page<>(page,limit);
        Map<String,Object> map=teacherService.getTeacherFrontList(pageTeacher);
        return R.ok().data(map);
    }
    /**
     * 根据id查询讲师信息
     */
    @GetMapping("getTeacherFrontInfo/{id}")
    public R getTeacherById(@PathVariable String id){
        EduTeacher eduTeacher = teacherService.getById(id);
        //根据讲师id查询所讲课程
        QueryWrapper<EduCourse> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("teacher_id",id);
        List<EduCourse> list = courseService.list(queryWrapper);
        return R.ok().data("teacher",eduTeacher).data("courseList",list);
    }
}
