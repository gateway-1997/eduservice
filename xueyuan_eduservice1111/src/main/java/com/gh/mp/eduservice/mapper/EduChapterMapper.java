package com.gh.mp.eduservice.mapper;

import com.gh.mp.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author Eric
 * @since 2021-04-25
 */
public interface EduChapterMapper extends BaseMapper<EduChapter> {

}
