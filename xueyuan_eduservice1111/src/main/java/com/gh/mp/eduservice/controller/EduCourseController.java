package com.gh.mp.eduservice.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gh.mp.edu.common.R;
import com.gh.mp.eduservice.entity.EduCourse;
import com.gh.mp.eduservice.entity.EduTeacher;
import com.gh.mp.eduservice.entity.form.CourseInfoForm;
import com.gh.mp.eduservice.entity.query.QueryCourse;
import com.gh.mp.eduservice.entity.vo.CoursePublishVo;
import com.gh.mp.eduservice.service.EduCourseService;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author Eric
 * @since 2021-04-24
 */
@RestController
@RequestMapping("/eduservice/edu-course")
@CrossOrigin
public class EduCourseController {

    @Autowired
    private EduCourseService eduCourseService;

    @PostMapping()
    public R addCourseInfo(@RequestBody CourseInfoForm courseInfoForm){
        String courseId=eduCourseService.insertCourseInfo(courseInfoForm);
        return R.ok().data("courseId",courseId);
    }

    /**
     * 根据课程id查询课程详细信息
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public R findCourseInfoById(@PathVariable String id){
        CourseInfoForm form=eduCourseService.getCourseInfoById(id);
        return R.ok().data("courseInfoForm",form);
    }

    @PutMapping("updateCourseInfo")
    public R saveUpdateCourseInfo(@RequestBody CourseInfoForm form){
        boolean flag=eduCourseService.updateCourseInfo(form);
        if(flag){
            return R.ok().data("courseId",form.getId());
        }else {
            return R.error();
        }
    }

    //分页查询课程列表的方法
    @GetMapping("pageList/{page}/{limit}")
    public R getPageCourseList(@PathVariable Long page,
                                @PathVariable Long limit){
        //创建page对象，创建参数
        Page<EduCourse> pageCourse=new Page<EduCourse>(page,limit);
        eduCourseService.page(pageCourse,null);
        //从pageTeacher获取数据
        long total = pageCourse.getTotal();//总条数
        List<EduCourse> records = pageCourse.getRecords();//总记录

        return R.ok().data("total",total).data("items",records);
    }

    @PostMapping("moreConditionPageList/{page}/{limit}")
    public R moreConditionPageList(@PathVariable Long page,
                                   @PathVariable Long limit,
                                   @RequestBody(required = false)QueryCourse queryCourse){
        IPage iPage = eduCourseService.moreConditionPageList(page, limit, queryCourse);
        long total = iPage.getTotal();
        List records = iPage.getRecords();
        return R.ok().data("total",total).data("items",records);
    }

    @DeleteMapping("deleteCourseById/{id}")
    public R deleteCourseInfoById(@PathVariable String id){
        Boolean flag=eduCourseService.deleteCourseById(id);
        if(flag){
            return R.ok();
        }
        return R.error().message("删除失败");
    }
    //获取课程信息
    @GetMapping("getAllCourseInfo/{id}")
    public R getAllCourseInfo(@PathVariable String id){
        //CoursePublishVo
        CoursePublishVo coursePublishVo=eduCourseService.getAllCourseInfo(id);
        return R.ok().data("items",coursePublishVo);
    }
    /**
     * 发布课程
     */
    @GetMapping("publishCourse/{id}")
    public R publishCourseByCourseId(@PathVariable String id){
       Boolean flag= eduCourseService.publishCourseByCourseId(id);
        if(flag){
            return R.ok();
        }
        return R.error();
    }


}

