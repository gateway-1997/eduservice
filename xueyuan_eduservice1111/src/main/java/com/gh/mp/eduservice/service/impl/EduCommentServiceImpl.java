package com.gh.mp.eduservice.service.impl;

import com.gh.mp.edu.common.R;
import com.gh.mp.edu.exception.EduException;
import com.gh.mp.edu.utils.JwtUtils;
import com.gh.mp.eduservice.client.UcenterClient;
import com.gh.mp.eduservice.entity.EduComment;
import com.gh.mp.eduservice.entity.UcenterMember;
import com.gh.mp.eduservice.entity.vo.CommentVo;
import com.gh.mp.eduservice.mapper.EduCommentMapper;
import com.gh.mp.eduservice.service.EduCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 * 评论 服务实现类
 * </p>
 *
 * @author Eric
 * @since 2021-05-19
 */
@Service
public class EduCommentServiceImpl extends ServiceImpl<EduCommentMapper, EduComment> implements EduCommentService {

    @Autowired
    private UcenterClient client;


    @Override
    public void addComment(CommentVo commentvo, HttpServletRequest request) {
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        if(StringUtils.isEmpty(memberId)) {
            throw new EduException(20001,"未登录");
        }
        commentvo.setMemberId(memberId);
        UcenterMember ucenterMember = client.getInfo(memberId);
        EduComment comment=new EduComment();
        comment.setMemberId(memberId);
        comment.setContent(commentvo.getContent());
        comment.setTeacherId(commentvo.getTeacherId());
        comment.setCourseId(commentvo.getCourseId());

        comment.setNickname(ucenterMember.getNickname());
        comment.setAvatar(ucenterMember.getAvatar());
        baseMapper.insert(comment);
    }

//    @Autowired
//    private UcenterClient ucenterClient;


//    @Override
//    public Boolean addComment(CommentVo commentVo) {
//        EduComment comment=new EduComment();
//        comment.setCourseId(commentVo.getCourseId());
//        comment.setTeacherId(commentVo.getTeacherId());
//        comment.setContent(commentVo.getContent());
//        try {
//            UcenterMember userInfo = ucenterClient.getMemberClientInfo(commentVo.getToken());
//            comment.setAvatar(userInfo.getAvatar());
//            comment.setNickname(userInfo.getNickname());
//            comment.setMemberId(userInfo.getId());
//            int i = baseMapper.insert(comment);
//            return i > 0;
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new RuntimeException();
//        }
//    }
}
