package com.gh.mp.eduservice.service.impl;

import com.gh.mp.eduservice.client.VodClient;
import com.gh.mp.eduservice.entity.EduVideo;
import com.gh.mp.eduservice.mapper.EduVideoMapper;
import com.gh.mp.eduservice.service.EduVideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author Eric
 * @since 2021-04-25
 */
@Service
public class EduVideoServiceImpl extends ServiceImpl<EduVideoMapper, EduVideo> implements EduVideoService {

    @Autowired
    private VodClient vodClient;

    @Override
    public EduVideo findVideoInfoById(String id) {
        EduVideo video = baseMapper.selectById(id);
        return video;
    }

    @Override
    public Boolean insertVideoInfo(EduVideo eduVideo) {
        int insert = baseMapper.insert(eduVideo);
        return insert > 0;
    }

    @Override
    public Boolean updateVideoById(EduVideo eduVideo) {
        int i = baseMapper.updateById(eduVideo);
        return i > 0;
    }

    @Override
    public Boolean deleteVideo(String id) {
        //同时删除阿里云视频
        //根据小节id获取视频id
        EduVideo video = baseMapper.selectById(id);
        String sourceId = video.getVideoSourceId();
        //根据视频id远程调用删除视频
        if(!StringUtils.isEmpty(sourceId)){
            vodClient.deleteAliyunVideo(sourceId);
        }
        int count = baseMapper.deleteById(id);
        return count > 0;
    }
}
