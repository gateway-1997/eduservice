package com.gh.mp.eduservice.client;

import com.gh.mp.edu.common.R;
import com.gh.mp.eduservice.entity.UcenterMember;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;


/**
 * @Author Eric
 * @Date 2021/5/20 0:01
 * @Version 1.0
 */
@Component
@FeignClient(name = "service-ucenter")
public interface UcenterClient {

//    @GetMapping("educenter/member/getMemberClientInfo")
//    UcenterMember getMemberClientInfo(@RequestParam("token") String token);


    @PostMapping("educenter/member/getInfoUc/{id}")
    UcenterMember getInfo(@PathVariable("id") String id);
}
