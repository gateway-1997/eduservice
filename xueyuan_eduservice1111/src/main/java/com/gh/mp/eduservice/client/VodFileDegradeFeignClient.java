package com.gh.mp.eduservice.client;

import com.gh.mp.edu.common.R;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author Eric
 * @Date 2021/5/8 0:44
 * @Version 1.0
 */
@Component
public class VodFileDegradeFeignClient implements VodClient {
    @Override
    public R deleteAliyunVideo(String videoId) {
        return R.error().data("msg","删除视频出错了");
    }

    @Override
    public R deleteBatch(List<String> videoIdList) {
        return R.error().data("msg","删除多个视频出错了");
    }
}
