package com.gh.mp.eduservice.controller;


import com.gh.mp.edu.common.R;
import com.gh.mp.eduservice.entity.EduChapter;
import com.gh.mp.eduservice.entity.EduVideo;
import com.gh.mp.eduservice.entity.form.CourseInfoForm;
import com.gh.mp.eduservice.service.EduVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author Eric
 * @since 2021-04-25
 */
@RestController
@RequestMapping("/eduservice/edu-video")
@CrossOrigin
public class EduVideoController {

    @Autowired
    private EduVideoService eduVideoService;

    /**
     * 通过id查找
     * @param id
     * @return
     */
    @GetMapping("findVideoInfoById/{id}")
    public R findVideoInfoById(@PathVariable String id) {
        EduVideo video = eduVideoService.findVideoInfoById(id);
        return R.ok().data("items", video);
    }

    /**
     * 添加小节
     * @param eduVideo
     * @return
     */
    @PostMapping("addVideo")
    public R addVideoInfo(@RequestBody EduVideo eduVideo){
        Boolean flag=eduVideoService.insertVideoInfo(eduVideo);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    /**
     * 修改小节信息
     * @param eduVideo
     * @return
     */
    @PutMapping("updateVideo")
    public R updateVideo(@RequestBody EduVideo eduVideo) {
        Boolean flag = eduVideoService.updateVideoById(eduVideo);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    /**
     * 删除小节信息
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public R deleteVideo(@PathVariable String id) {
        Boolean flag = eduVideoService.deleteVideo(id);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }
}

