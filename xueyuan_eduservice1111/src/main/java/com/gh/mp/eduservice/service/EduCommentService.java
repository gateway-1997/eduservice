package com.gh.mp.eduservice.service;

import com.gh.mp.eduservice.entity.EduComment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gh.mp.eduservice.entity.vo.CommentVo;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 评论 服务类
 * </p>
 *
 * @author Eric
 * @since 2021-05-19
 */
public interface EduCommentService extends IService<EduComment> {
    void addComment(CommentVo commentvo, HttpServletRequest request);

//    Boolean addComment(CommentVo commentVo);
}
