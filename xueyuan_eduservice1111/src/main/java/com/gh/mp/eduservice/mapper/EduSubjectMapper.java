package com.gh.mp.eduservice.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gh.mp.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import javax.security.auth.Subject;
import java.util.List;

/**
 * <p>
 * 课程科目 Mapper 接口
 * </p>
 *
 * @author Eric
 * @since 2021-04-19
 */
public interface EduSubjectMapper extends BaseMapper<EduSubject> {

    List<EduSubject> selectList(QueryWrapper<Subject> queryWrapper2);
}
