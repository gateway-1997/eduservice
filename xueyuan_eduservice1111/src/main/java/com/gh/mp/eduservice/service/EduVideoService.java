package com.gh.mp.eduservice.service;

import com.gh.mp.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author Eric
 * @since 2021-04-25
 */
public interface EduVideoService extends IService<EduVideo> {

    EduVideo findVideoInfoById(String id);

    Boolean insertVideoInfo(EduVideo eduVideo);

    Boolean updateVideoById(EduVideo eduVideo);

    Boolean deleteVideo(String id);
}
