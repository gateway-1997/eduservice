package com.gh.mp.eduservice.service;

import com.gh.mp.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gh.mp.eduservice.entity.vo.SubjectNestedVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author Eric
 * @since 2021-04-19
 */
public interface EduSubjectService extends IService<EduSubject> {

    /**
     * 读取Excel内容
     * @param multipartFile
     */
    List<String> importExcelSubject(MultipartFile multipartFile);

    /**
     * 查询树形分类列表
     * @return
     */
    List<SubjectNestedVo> nestedList();

    boolean deleteSubjectById(String id);

    boolean insertSubject(EduSubject eduSubject);
}
