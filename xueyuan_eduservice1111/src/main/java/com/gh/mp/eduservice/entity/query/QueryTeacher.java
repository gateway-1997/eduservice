package com.gh.mp.eduservice.entity.query;

import lombok.Data;

/**
 * @Author Eric
 * @Date 2021/4/3 22:09
 * @Version 1.0
 **/
@Data
public class QueryTeacher {

    private String name;
    private String level;
    private String startTime;
    private String endTime;
}
