package com.gh.mp.eduservice.client;

import com.aliyun.oss.ClientException;
import com.gh.mp.edu.common.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Author Eric
 * @Date 2021/5/6 23:18
 * @Version 1.0
 */
@Component
@FeignClient(name = "xueyuan-vidservice",fallback = VodFileDegradeFeignClient.class)
public interface VodClient {
    //定义调用方法的路径
    @DeleteMapping("/vidservice/vod/{videoId}")
    public R deleteAliyunVideo(@PathVariable("videoId") String videoId);

    @DeleteMapping("/vidservice/vod/delete-batch")
    public R deleteBatch(@RequestParam("videoIdList") List<String> videoIdList);
}
