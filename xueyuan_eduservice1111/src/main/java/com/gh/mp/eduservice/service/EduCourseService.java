package com.gh.mp.eduservice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gh.mp.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gh.mp.eduservice.entity.form.CourseInfoForm;
import com.gh.mp.eduservice.entity.frontVo.CourseFrontVo;
import com.gh.mp.eduservice.entity.frontVo.CourseInfoFrontVo;
import com.gh.mp.eduservice.entity.query.QueryCourse;
import com.gh.mp.eduservice.entity.vo.CoursePublishVo;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author Eric
 * @since 2021-04-24
 */
public interface EduCourseService extends IService<EduCourse> {

    String insertCourseInfo(CourseInfoForm courseInfoForm);

    CourseInfoForm getCourseInfoById(String id);

    boolean updateCourseInfo(CourseInfoForm form);

    IPage moreConditionPageList(Long page, Long limit, QueryCourse queryCourse);

    Boolean deleteCourseById(String id);

    CoursePublishVo getAllCourseInfo(String id);

    Boolean publishCourseByCourseId(String id);

    Map<String, Object> getFrontCourseList(Page<EduCourse> pageCourse, CourseFrontVo courseFrontVo);

    CourseInfoFrontVo getFrontCourseInfo(String id);

    Boolean isBuy(String courseId, HttpServletRequest request);
}
