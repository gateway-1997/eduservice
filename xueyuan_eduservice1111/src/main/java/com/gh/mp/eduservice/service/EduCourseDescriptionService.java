package com.gh.mp.eduservice.service;

import com.gh.mp.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author Eric
 * @since 2021-04-24
 */
public interface EduCourseDescriptionService extends IService<EduCourseDescription> {

}
