package com.gh.mp.eduservice.service;

import com.gh.mp.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gh.mp.eduservice.entity.dto.EduChapterDto;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author Eric
 * @since 2021-04-25
 */
public interface EduChapterService extends IService<EduChapter> {

    List<EduChapterDto> getChapterVideoList(String id);

    Boolean addChapter(EduChapter eduChapter);

    Boolean updateChapterById(EduChapter eduChapter);

    EduChapter findChapterInfoById(String id);

    Boolean deleteChapter(String id);
}
