package com.gh.mp.eduservice.mapper;

import com.gh.mp.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gh.mp.eduservice.entity.frontVo.CourseInfoFrontVo;
import com.gh.mp.eduservice.entity.vo.CoursePublishVo;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author Eric
 * @since 2021-04-24
 */
@Repository
public interface EduCourseMapper extends BaseMapper<EduCourse> {

    CoursePublishVo getAllCourseInfo(String id);

    CourseInfoFrontVo getFrontCourseInfo(String id);
}
