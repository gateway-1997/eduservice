package com.gh.mp.eduservice.controller;


import com.gh.mp.edu.common.R;
import com.gh.mp.eduservice.entity.EduChapter;
import com.gh.mp.eduservice.entity.dto.EduChapterDto;
import com.gh.mp.eduservice.service.EduChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author Eric
 * @since 2021-04-25
 */
@RestController
@RequestMapping("/eduservice/edu-chapter")
@CrossOrigin
public class EduChapterController {

    @Autowired
    private EduChapterService chapterService;

    @GetMapping("getChapterVideoList/{id}")
    public R getChapterVideoList(@PathVariable String id) {
        List<EduChapterDto> list = chapterService.getChapterVideoList(id);
        return R.ok().data("items", list);
    }

    /**
     * 添加章节信息
     *
     * @param eduChapter
     * @return
     */
    @PostMapping("addChapter")
    public R addChapter(@RequestBody EduChapter eduChapter) {
        Boolean flag = chapterService.addChapter(eduChapter);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    /**
     * 修改章节信息
     *
     * @param eduChapter
     * @return
     */
    @PutMapping("updateChapter")
    public R updateChapter(@RequestBody EduChapter eduChapter) {
        Boolean flag = chapterService.updateChapterById(eduChapter);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @GetMapping("findChapterInfoById/{id}")
    public R findChapterInfoById(@PathVariable String id) {
        EduChapter chapter = chapterService.findChapterInfoById(id);
        return R.ok().data("items", chapter);
    }

    @DeleteMapping("{id}")
    public R deleteChapter(@PathVariable String id) {
        Boolean flag = chapterService.deleteChapter(id);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }

}

