package com.gh.mp.eduservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gh.mp.edu.common.R;
import com.gh.mp.edu.utils.JwtUtils;
import com.gh.mp.eduservice.client.UcenterClient;
import com.gh.mp.eduservice.entity.EduComment;
import com.gh.mp.eduservice.entity.UcenterMember;
import com.gh.mp.eduservice.entity.vo.CommentVo;
import com.gh.mp.eduservice.service.EduCommentService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 评论 前端控制器
 * </p>
 *
 * @author Eric
 * @since 2021-05-19
 */
@RestController
@RequestMapping("/eduservice/edu-comment")
@CrossOrigin
public class EduCommentController {

    @Autowired
    private EduCommentService eduCommentService;

    @Autowired
    private UcenterClient client;
    /**
     * 分页显示评论
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("findComment/{page}/{limit}")
    public R findCommentByPage(@PathVariable Long page,
                               @PathVariable Long limit,
                               @RequestParam(required = false) String courseId){
        Page<EduComment> pageParam=new Page<>(page,limit);
        QueryWrapper<EduComment> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",courseId);
        Map<String, Object> map = new HashMap<>();
        eduCommentService.page(pageParam,wrapper);
        map.put("items", pageParam.getRecords());
        map.put("current", pageParam.getCurrent());
        map.put("pages", pageParam.getPages());
        map.put("size", pageParam.getSize());
        map.put("total", pageParam.getTotal());
        map.put("hasNext", pageParam.hasNext());
        map.put("hasPrevious", pageParam.hasPrevious());
        return R.ok().data(map);
    }

//    @PostMapping("addComment")
//    public R addComment(@RequestBody CommentVo commentVo){
//        Boolean flag = eduCommentService.addComment(commentVo);
//
//        return R.ok();
//    }

    @ApiOperation(value = "添加评论")
    @PostMapping("addComment")
    public R save(@RequestBody CommentVo commentvo, HttpServletRequest request) {

        eduCommentService.addComment(commentvo,request);

        return R.ok();
    }
}

