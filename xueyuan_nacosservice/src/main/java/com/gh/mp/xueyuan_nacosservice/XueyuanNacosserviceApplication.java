package com.gh.mp.xueyuan_nacosservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class XueyuanNacosserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(XueyuanNacosserviceApplication.class, args);
    }

}
