package com.gh.mp.edu.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author Eric
 * @Date 2021/4/5 21:05
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EduException extends RuntimeException {

    private Integer code;//状态码

    private String message;//异常信息

}
