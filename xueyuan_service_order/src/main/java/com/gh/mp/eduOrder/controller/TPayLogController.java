package com.gh.mp.eduOrder.controller;


import com.gh.mp.edu.common.R;
import com.gh.mp.eduOrder.service.TPayLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 支付日志表 前端控制器
 * </p>
 *
 * @author Eric
 * @since 2021-05-21
 */
@RestController
@RequestMapping("/eduOrder/paylog")
@CrossOrigin
public class TPayLogController {

    @Autowired
    private TPayLogService tPayLogService;

    /**
     * 生成微信二维码接口,参数是订单号
     * @param orderNo
     * @return
     */
    @GetMapping("createNative/{orderNo}")
    public R createNative(@PathVariable String orderNo){
        Map map= tPayLogService.createNative(orderNo);
        return R.ok().data(map);
    }

    /**
     * 根据订单id查询支付状态
     * @param orderNo
     * @return
     */
    @GetMapping("queryPayStatus/{orderNo}")
    public R queryPayStatus(@PathVariable String orderNo){
        Map<String,String> map = tPayLogService.queryPayStatus(orderNo);
        if(map==null){
            return R.error().data("msg","支付出错了");
        }
        //如果返回的map不为空
        if(map.get("trade_state").equals("SUCCESS")){
            //添加记录到支付表，同时修改订单状态
            tPayLogService.updateOrderState(map);
            return R.ok().message("支付成功");
        }
        return R.ok().code(25000).message("支付中");
    }
}

