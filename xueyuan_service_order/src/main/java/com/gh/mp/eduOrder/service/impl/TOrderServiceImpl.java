package com.gh.mp.eduOrder.service.impl;

import com.gh.mp.eduOrder.client.EduClient;
import com.gh.mp.eduOrder.client.UcenterClient;
import com.gh.mp.eduOrder.entity.TOrder;
import com.gh.mp.eduOrder.entity.vo.CourseInfoFrontVo;
import com.gh.mp.eduOrder.entity.vo.UcenterMember;
import com.gh.mp.eduOrder.mapper.TOrderMapper;
import com.gh.mp.eduOrder.service.TOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gh.mp.eduOrder.utils.OrderNoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.OrderUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author Eric
 * @since 2021-05-21
 */
@Service
public class TOrderServiceImpl extends ServiceImpl<TOrderMapper, TOrder> implements TOrderService {

    @Autowired
    private EduClient eduClient;

    @Autowired
    private UcenterClient ucenterClient;

    @Override
    public String createOrder(String courseId, String memberId) {
        //通过远程调用根据用户id获取用户信息
        UcenterMember ucenterMember = ucenterClient.getInfo(memberId);
        //通过远程调用根据课程id获取课程信息
        CourseInfoFrontVo courseOrder = eduClient.getFrontCourseOrder(courseId);
        TOrder order = new TOrder();
        String orderNo = OrderNoUtil.getOrderNo();
        order.setOrderNo(orderNo);//订单号
        order.setOrderNo(OrderNoUtil.getOrderNo());
        order.setCourseId(courseId);
        order.setCourseTitle(courseOrder.getTitle());
        order.setCourseCover(courseOrder.getCover());
        order.setTeacherName(courseOrder.getTeacherName());
        order.setTotalFee(courseOrder.getPrice());
        order.setMemberId(memberId);
        order.setMobile(ucenterMember.getMobile());
        order.setNickname(ucenterMember.getNickname());
        order.setStatus(0);// 订单状态 0:未支付 1:已支付
        order.setPayType(1);//支付类型 微信
        baseMapper.insert(order);
        return order.getOrderNo();
    }
}
