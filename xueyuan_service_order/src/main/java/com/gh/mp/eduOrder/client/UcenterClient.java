package com.gh.mp.eduOrder.client;

import com.gh.mp.eduOrder.entity.vo.UcenterMember;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Component
@FeignClient(name = "service-ucenter")
public interface UcenterClient {

    @PostMapping("/educenter/member/getInfoUc/{id}")
    UcenterMember getInfo(@PathVariable("id") String id);
}
