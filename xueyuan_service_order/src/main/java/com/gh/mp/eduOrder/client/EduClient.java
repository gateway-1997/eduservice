package com.gh.mp.eduOrder.client;

import com.gh.mp.eduOrder.entity.vo.CourseInfoFrontVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @Author Eric
 * @Date 2021/5/22 11:33
 * @Version 1.0
 */
@Component
@FeignClient(name = "xueyuan-edu")
public interface EduClient {

    @PostMapping("/eduservice/coursefront/getCourseOrderById/{id}")
    CourseInfoFrontVo getFrontCourseOrder(@PathVariable("id") String id);
}
