package com.gh.mp.eduOrder.service;

import com.gh.mp.eduOrder.entity.TOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author Eric
 * @since 2021-05-21
 */
public interface TOrderService extends IService<TOrder> {

    String createOrder(String courseId, String memberId);
}
