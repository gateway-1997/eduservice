package com.gh.mp.eduOrder.mapper;

import com.gh.mp.eduOrder.entity.TPayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付日志表 Mapper 接口
 * </p>
 *
 * @author Eric
 * @since 2021-05-21
 */
public interface TPayLogMapper extends BaseMapper<TPayLog> {

}
