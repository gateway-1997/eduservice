package com.gh.mp.eduOrder.mapper;

import com.gh.mp.eduOrder.entity.TOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author Eric
 * @since 2021-05-21
 */
public interface TOrderMapper extends BaseMapper<TOrder> {

}
