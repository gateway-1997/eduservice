package com.gh.mp.eduOrder.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gh.mp.edu.common.R;
import com.gh.mp.edu.utils.JwtUtils;
import com.gh.mp.eduOrder.entity.TOrder;
import com.gh.mp.eduOrder.service.TOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单 前端控制器
 * </p>
 *
 * @author Eric
 * @since 2021-05-21
 */
@RestController
@RequestMapping("/eduOrder/t-order")
@CrossOrigin
public class TOrderController {

    @Autowired
    private TOrderService tOrderService;

    //生成订单的方法
    @PostMapping("createOrder/{courseId}")
    public R createOrder(@PathVariable String courseId, HttpServletRequest request){
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        //返回订单号
        String orderNo = tOrderService.createOrder(courseId,memberId);

        return R.ok().data("orderNo",orderNo);
    }
    /**
     * 根据订单id查询订单信息
     */
    @GetMapping("getOrderInfo/{orderId}")
    public R getOrderInfo(@PathVariable String orderId){
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("order_no",orderId);
        TOrder order = tOrderService.getOne(queryWrapper);
        return R.ok().data("order",order);
    }
    /**
     * 根据课程id和用户id查询订单表中的状态
     */

    @GetMapping("isBuyCourse/{courseId}/{memberId}")
    public boolean isBuyCourse(@PathVariable String courseId,@PathVariable String memberId){
        QueryWrapper<TOrder> wrapper = new QueryWrapper<TOrder>();
        wrapper.eq("course_id",courseId);
        wrapper.eq("member_id",memberId);
        wrapper.eq("status",1);
        int count = tOrderService.count(wrapper);
        if(count == 0){
            return false;
        }
        return true;
    }

}

