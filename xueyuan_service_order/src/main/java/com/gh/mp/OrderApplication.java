package com.gh.mp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Author Eric
 * @Date 2021/5/21 22:52
 * @Version 1.0
 */
@SpringBootApplication
@ComponentScan({"com.gh.mp.eduOrder"})
@MapperScan("com.gh.mp.eduOrder.mapper")
@EnableDiscoveryClient
@EnableFeignClients
@EnableSwagger2
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class,args);
    }
}
