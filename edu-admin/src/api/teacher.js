import request from '@/utils/request'

export function getTeacherPageList(page, limit ,searchObj) {
  return request({
    url: '/eduservice/edu-teacher/moreConditionPageList/'+page+'/'+limit,
    method: 'post',
    data: searchObj
  })
}
//删除
export function removeTeacherById(id) {
  return request({
    url: '/eduservice/edu-teacher/'+id,
    method: 'delete',
  })
}
//添加
export function saveTeacher(teacher) {
  return request({
    url: '/eduservice/edu-teacher/addTeacher',
    method: 'post',
    data: teacher
  })
}
//修改
export function updateTeacher(teacher) {
  return request({
    url: '/eduservice/edu-teacher/updateTeacher',
    method: 'put',
    data: teacher
  })
}
//通过id查询
export function findTeacherById(id) {
  return request({
    url: '/eduservice/edu-teacher/getTeacherInfo/'+id,
    method: 'get',
  })
}
//查询所有讲师
export function findAllTeacher() {
  return request({
    url: '/eduservice/edu-teacher',
    method: 'get',
  })
}
