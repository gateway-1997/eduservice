import request from '@/utils/request'

const URL='/eduservice/edu-chapter'

export function getChapterVideoList(id) {
  return request({
    url: URL+'/getChapterVideoList/'+id,
    method: 'get'
  })
}

export function addChapter(chapter) {
    return request({
      url: URL+'/addChapter',
      method: 'post',
      data: chapter
    })
}

export function findChapterById(id) {
    return request({
      url: URL+'/findChapterInfoById/'+id,
      method: 'get'
    })
}

export function updateChapter(chapter) {
    return request({
      url: URL+'/updateChapter',
      method: 'put',
      data: chapter
    })
}

export function deleteChapterById(id) {
    return request({
      url: URL+'/'+id,
      method: 'delete'
    })
}

