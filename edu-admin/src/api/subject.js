import request from '@/utils/request'
const api_name = '/eduservice/edu-subject/'

export function getNestedTreeList() {
    return request({
      url: `${api_name}`,
      method: 'get',
    })
  }
  export function removeSubjectId(id) {
    return request({
      url: api_name+id,
      method: 'delete',
    })
  }
  export function insertSubject(subject) {
    return request({
      url: api_name+'insertSubject',
      method: 'post',
      data:subject
    })
  }