import request from '@/utils/request'

export function addCourseInfo(courseInfo) {
    return request({
        url: '/eduservice/edu-course',
        method: 'post',
        data: courseInfo
    })
}
export function findCourseInfoById(id) {
    return request({
        url: '/eduservice/edu-course/'+id,
        method: 'get',
    })
}
export function saveUpdateDate(courseInfo){
    return request({
        url: '/eduservice/edu-course/updateCourseInfo',
        method: 'put',
        data: courseInfo
    })
}
export function findAllCoursePage(page,limit) {
    return request({
        url: `/eduservice/edu-course/pageList/${page}/${limit}`,
        method: 'get',
    })
}

export function searchObjAllCoursePage(page,limit,searchObj) {
    return request({
        url: '/eduservice/edu-course/moreConditionPageList/'+page+'/'+limit,
        method: 'post',
        data: searchObj
    })
}

export function removeCourseById(id) {
    return request({
        url: '/eduservice/edu-course/deleteCourseById/'+id,
        method: 'delete'
    })
}

