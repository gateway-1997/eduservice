import request from '@/utils/request'

const URL='/eduservice/edu-video'

export function addVideo(video) {
    return request({
      url: URL+'/addVideo',
      method: 'post',
      data: video
    })
}

export function findVideoById(id) {
    return request({
      url: URL+'/findVideoInfoById/'+id,
      method: 'get'
    })
}

export function updateVideo(video) {
    return request({
      url: URL+'/updateVideo',
      method: 'put',
      data: video
    })
}

export function deleteVideoById(id) {
    return request({
      url: URL+'/'+id,
      method: 'delete'
    })
}
export function deleteAliyunVideo(id) {
  return request({
    url: `/vidservice/vod/${id}`,
    method: 'delete'
  })
}