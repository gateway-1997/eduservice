import request from '@/utils/request'
//生成统计数据
export function createStaData(day) {
  return request({
    url: `/staservice/statistics-daily/registerCount/${day}`,
    method: 'get'
  })
}
//查询统计数据
export function getData(searchObj) {
  return request({
    url: `/staservice/statistics-daily/showData/${searchObj.type}/${searchObj.begin}/${searchObj.end}`,
    method: 'get'
  })
}