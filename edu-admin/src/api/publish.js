import request from '@/utils/request'

export function findInfoById(courseId) {
  return request({
    url: '/eduservice/edu-course/getAllCourseInfo/'+courseId,
    method: 'get',
  })
}

export function publishCourse(courseId) {
  return request({
    url: '/eduservice/edu-course/publishCourse/'+courseId,
    method: 'get',
  })
}