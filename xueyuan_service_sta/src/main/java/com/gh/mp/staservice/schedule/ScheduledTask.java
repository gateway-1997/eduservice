package com.gh.mp.staservice.schedule;

import com.gh.mp.staservice.service.StatisticsDailyService;
import com.gh.mp.staservice.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author Eric
 * @Date 2021/5/23 21:57
 * @Version 1.0
 */
@Component
public class ScheduledTask {

    @Autowired
    private StatisticsDailyService statisticsDailyService;

    /**
     * 每隔五秒去执行这个方法
     */
//    @Scheduled(cron = "0/5 * * * *?")
//    public void task1() {
//        System.out.println("task1执行了");
//    }

    /**
     * 在每天凌晨零点把前一天的数据进行添加
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void task2() {
        String day = DateUtil.formatDate(DateUtil.addDays(new Date(), -1));
        statisticsDailyService.registerCount(day);
    }
}
