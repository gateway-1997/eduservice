package com.gh.mp.staservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Author Eric
 * @Date 2021/5/23 15:11
 * @Version 1.0
 */
@SpringBootApplication
@ComponentScan({"com.gh.mp"})
@MapperScan("com.gh.mp.staservice.mapper")
@EnableDiscoveryClient
@EnableFeignClients
@EnableSwagger2
@EnableScheduling
public class SatApplication {

    public static void main(String[] args) {
        SpringApplication.run(SatApplication.class,args);
    }
}
