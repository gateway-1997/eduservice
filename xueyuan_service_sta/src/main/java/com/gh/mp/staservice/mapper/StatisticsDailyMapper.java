package com.gh.mp.staservice.mapper;

import com.gh.mp.staservice.entity.StatisticsDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网站统计日数据 Mapper 接口
 * </p>
 *
 * @author Eric
 * @since 2021-05-23
 */
public interface StatisticsDailyMapper extends BaseMapper<StatisticsDaily> {

    void deleteByDay(String day);
}
