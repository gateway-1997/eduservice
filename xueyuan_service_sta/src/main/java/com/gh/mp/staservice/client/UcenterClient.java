package com.gh.mp.staservice.client;

import com.gh.mp.edu.common.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "service-ucenter")//调用的服务名称
@Component
public interface UcenterClient {

    @GetMapping("/educenter/member/countRegister/{day}")
    R countRegister(@PathVariable("day") String day);
}


