package com.gh.mp.staservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gh.mp.edu.common.R;
import com.gh.mp.staservice.client.UcenterClient;
import com.gh.mp.staservice.entity.StatisticsDaily;
import com.gh.mp.staservice.mapper.StatisticsDailyMapper;
import com.gh.mp.staservice.service.StatisticsDailyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务实现类
 * </p>
 *
 * @author Eric
 * @since 2021-05-23
 */
@Service
public class StatisticsDailyServiceImpl extends ServiceImpl<StatisticsDailyMapper, StatisticsDaily> implements StatisticsDailyService {

    @Autowired
    private UcenterClient client;

    @Autowired
    private StatisticsDailyMapper statisticsDailyMapper;

    public void registerCount(String day) {
        //添加之前先删除表相同的数据
        statisticsDailyMapper.deleteByDay(day);

        R r = client.countRegister(day);
        Integer count = (Integer) r.getData().get("count");
        Integer loginNum = RandomUtils.nextInt(100, 200);//TODO
        Integer videoViewNum = RandomUtils.nextInt(100, 200);//TODO
        Integer courseNum = RandomUtils.nextInt(100, 200);//TODO
        //创建统计对象
        StatisticsDaily daily = new StatisticsDaily();
        daily.setRegisterNum(count);
        daily.setLoginNum(loginNum);
        daily.setVideoViewNum(videoViewNum);
        daily.setCourseNum(courseNum);
        daily.setDateCalculated(day);
        baseMapper.insert(daily);
    }

    @Override
    public Map<String, Object> showData(String type, String begin, String end) {
        QueryWrapper<StatisticsDaily> queryWrapper=new QueryWrapper<StatisticsDaily>();
        queryWrapper.between("date_calculated",begin,end);
        queryWrapper.select("date_calculated",type);
        List<StatisticsDaily> list = baseMapper.selectList(queryWrapper);
        List<String> date=new ArrayList<String>();//日期
        List<Integer> number=new ArrayList<Integer>();//数量
        list.forEach(i->{
            String s = i.getDateCalculated();
            date.add(s);
            //封装对应数量
            switch (type){
                case "login_num":
                    number.add(i.getLoginNum());
                    break;
                case "register_num":
                    number.add(i.getRegisterNum());
                    break;
                case "video_view_num":
                    number.add(i.getVideoViewNum());
                    break;
                case "course_num":
                    number.add(i.getCourseNum());
                    break;
                default:
                    break;
            }
        });
        Map<String,Object> map=new HashMap<>();
        map.put("date",date);
        map.put("number",number);
        return map;
    }

}
