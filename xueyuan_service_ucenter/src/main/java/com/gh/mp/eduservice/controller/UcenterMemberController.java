package com.gh.mp.eduservice.controller;


import com.gh.mp.edu.common.R;
import com.gh.mp.edu.utils.JwtUtils;
import com.gh.mp.eduservice.entity.UcenterMember;
import com.gh.mp.eduservice.entity.vo.RegistVo;
import com.gh.mp.eduservice.service.UcenterMemberService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author Eric
 * @since 2021-05-15
 */
@RestController
@RequestMapping("/educenter/member")
@CrossOrigin
public class UcenterMemberController {

    @Autowired
    private UcenterMemberService ucenterMemberService;

    /**
     * 用户登录
     * @param member
     * @return
     */
    @PostMapping("login")
    public R login(@RequestBody UcenterMember member){
       String token = ucenterMemberService.login(member);
       return R.ok().data("token",token);
    }

    /**
     * 用户注册
     * @param registVo
     * @return
     */
    @PostMapping("register")
    public R registerUser(@RequestBody RegistVo registVo){
        ucenterMemberService.register(registVo);
        return R.ok();
    }

    /**
     * 根据token获取用户信息
     * @param request
     * @return
     */
    @GetMapping("getMemberInfo")
    public R getMemberInfo(HttpServletRequest request){
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        //根据用户id获取用户信息
        UcenterMember member = ucenterMemberService.getById(memberId);
        return R.ok().data("userInfo",member);
    }
    @GetMapping("getMemberClientInfo")
    public UcenterMember getMemberClientInfo(String token){
        // 问题在这里，两个服务没用做权限共享，这里的request没用登录信息怎么做，做法太多了，超级复杂
        // 最简单的方法是在网关整合Security，但你现在还没学到网关


        String memberId = JwtUtils.getMemberIdByToken(token);
        //根据用户id获取用户信息
        UcenterMember member = ucenterMemberService.getById(memberId);
        return member;
    }

    //根据用户id获取用户信息
    @PostMapping("getInfoUc/{id}")
    public UcenterMember getInfo(@PathVariable String id) {

        UcenterMember ucenterMember = ucenterMemberService.getById(id);
        UcenterMember member = new UcenterMember();
        BeanUtils.copyProperties(ucenterMember,member);
        return member;
    }
    /**
     * 查询某一天注册人数
     */
    @GetMapping("countRegister/{day}")
    public R countRegister(@PathVariable String day){
        Integer count = ucenterMemberService.countRegister(day);
        return R.ok().data("count",count);
    }
}

