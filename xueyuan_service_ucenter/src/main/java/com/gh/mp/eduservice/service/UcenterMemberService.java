package com.gh.mp.eduservice.service;

import com.gh.mp.eduservice.entity.UcenterMember;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gh.mp.eduservice.entity.vo.RegistVo;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author Eric
 * @since 2021-05-15
 */
public interface UcenterMemberService extends IService<UcenterMember> {

    String login(UcenterMember member);

    void register(RegistVo registVo);

    UcenterMember getOpenIdMember(String openid);

    Integer countRegister(String day);
}
