package com.gh.mp.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gh.mp.edu.exception.EduException;
import com.gh.mp.edu.utils.JwtUtils;
import com.gh.mp.edu.utils.Md5Utils;
import com.gh.mp.eduservice.entity.UcenterMember;
import com.gh.mp.eduservice.entity.vo.RegistVo;
import com.gh.mp.eduservice.mapper.UcenterMemberMapper;
import com.gh.mp.eduservice.service.UcenterMemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author Eric
 * @since 2021-05-15
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Autowired
    private UcenterMemberMapper memberMapper;

    public String login(UcenterMember member) {
        //获取登录的手机号和密码
        String mobile = member.getMobile();
        String password = member.getPassword();
        if(StringUtils.isEmpty(mobile)||StringUtils.isEmpty(password)){
            throw new EduException(20001,"用户名或密码不能为空");
        }
        QueryWrapper<UcenterMember> queryWrapper=new QueryWrapper<UcenterMember>();
        queryWrapper.eq("mobile",mobile);
        UcenterMember ucenterMember = baseMapper.selectOne(queryWrapper);
        if(ucenterMember==null){//没有这个手机号
            throw new EduException(20001,"手机号无效");
        }
        //判断密码,首先把输入的密码进行加密，然后再跟数据库密码进行比较
        String md5Password = Md5Utils.md5(password);
        if(!md5Password.equals(ucenterMember.getPassword())){
            throw new EduException(20001,"密码错误");
        }
        if(ucenterMember.getIsDisabled()){
            throw new EduException(20001,"此账号被禁用");
        }
        //使用jwt生成token
        String jwtToken = JwtUtils.getJwtToken(ucenterMember.getId(), ucenterMember.getNickname());
        return jwtToken;
    }

    public void register(RegistVo registVo) {
        String mobile = registVo.getMobile();//手机号
        String nickname = registVo.getNickname();//昵称
        String password = registVo.getPassword();//密码
        String code = registVo.getCode();//验证码
        if(StringUtils.isEmpty(mobile)||StringUtils.isEmpty(nickname)||StringUtils.isEmpty(password)||StringUtils.isEmpty(code)){
            throw new EduException(20001,"注册失败");
        }
        String redisCode = redisTemplate.opsForValue().get(mobile);
        if(!code.equalsIgnoreCase(redisCode)){
            throw new EduException(20001,"验证码不正确");
        }
        //判断手机号是否重复
        QueryWrapper<UcenterMember> queryWrapper=new QueryWrapper<UcenterMember>();
        queryWrapper.eq("mobile",mobile);
        Integer count = baseMapper.selectCount(queryWrapper);
        if(count>0){
            throw new EduException(20001,"手机号重复");
        }
        UcenterMember member=new UcenterMember();
        member.setNickname(nickname);
        member.setPassword(Md5Utils.md5(password));
        member.setMobile(mobile);
        member.setIsDisabled(false);
        member.setAvatar("http://eric123.oss-cn-shanghai.aliyuncs.com/2021/04/24/career/bcd36ad4-4e84-472b-b52d-6beefe43880afile.png");
        baseMapper.insert(member);
    }

    @Override
    public UcenterMember getOpenIdMember(String openid) {
        QueryWrapper<UcenterMember> queryWrapper=new QueryWrapper<UcenterMember>();
        queryWrapper.eq("openid",openid);
        UcenterMember member = baseMapper.selectOne(queryWrapper);
        return member;
    }

    @Override
    public Integer countRegister(String day) {
        int count = memberMapper.countRegister(day);
        return count;
    }
}
