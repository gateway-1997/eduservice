package com.gh.mp.eduservice.mapper;

import com.gh.mp.eduservice.entity.UcenterMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author Eric
 * @since 2021-05-15
 */
public interface UcenterMemberMapper extends BaseMapper<UcenterMember> {

    int countRegister(String day);
}
