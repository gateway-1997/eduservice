import request from '@/utils/request'
//查询所有课程
export function getCourseList(page, limit, searchObj) {
    return request({
        url: `/eduservice/coursefront/getFrontCourseList/${page}/${limit}`,
        method: 'post',
        data: searchObj
    })
}
//查询所有分类
export function getAllSubject() {
    return request({
        url: '/eduservice/edu-subject',
        method: 'get'
    })
}
//根据id查询详情
export function getCourseInfoById(id) {
    return request({
        url: '/eduservice/coursefront/getCourseInfoById/'+id,
        method: 'post'
    })
}
//添加评论
export function addComment(commentForm) {
    return request({
        url: '/eduservice/edu-comment/addComment',
        method: 'post',
        data: commentForm
    })
}

export function getPageList(page, limit, courseId) {
    return request({
      url: `/eduservice/edu-comment/findComment/${page}/${limit}`,
      method: 'get',
      params: {courseId}
    })
  }