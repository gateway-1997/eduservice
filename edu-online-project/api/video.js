import request from '@/utils/request'

//根据id查询详情
export function getPlayAuth(id) {
    return request({
        url: '/vidservice/vod/getPlayAuth/'+id,
        method: 'get'
    })
}