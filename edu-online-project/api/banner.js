import request from '@/utils/request'

const URL='/educms/bannerFront'

export function getAllBanner() {
  return request({
    url: URL+'/getAllBanner',
    method: 'get'
  })
}
