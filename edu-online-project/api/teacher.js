import request from '@/utils/request'

export function getTeacherList(page,limit) {
    return request({
        url: `/eduservice/index-front/getFrontTeacherList/${page}/${limit}`,
        method: 'post'
    })
}

export function getTeacherInfo(id) {
    return request({
        url: `/eduservice/index-front/getTeacherFrontInfo/${id}`,
        method: 'get'
    })
}