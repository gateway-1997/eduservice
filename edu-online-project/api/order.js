import request from '@/utils/request'

const URL='/eduOrder/t-order'

export function createOrder(id) {
  return request({
    url: URL+'/createOrder/'+id,
    method: 'post'
  })
}

export function getOrderInfo(id) {
    return request({
      url: URL+'/getOrderInfo/'+id,
      method: 'get'
    })
  }
  //生成二维码
export function createNative(orderNo) {
    return request({
      url: `/eduOrder/paylog/createNative/${orderNo}`,
      method: 'get'
    })
  }
//根据订单id查询支付状态
export function queryPayStatus(orderNo) {
    return request({
      url: `/eduOrder/paylog/queryPayStatus/${orderNo}`,
      method: 'get'
    })
  }
