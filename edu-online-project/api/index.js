import request from '@/utils/request'

const URL='/eduservice/index-front'

export function getIndexData() {
  return request({
    url: URL+'/index',
    method: 'get'
  })
}
