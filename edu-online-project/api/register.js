import request from '@/utils/request'

export function getCode(phone) {
    return request({
        url: "/edumsm/msm/send/" + phone,
        method: 'get'
    })
}

export function userRegister(formItem) {
    return request({
        url: "/educenter/member/register",
        method: 'post',
        data: formItem
    })
}