package com.gh.mp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Author Eric
 * @Date 2021/5/27 21:48
 * @Version 1.0
 */
@SpringBootApplication
@ComponentScan({"com.gh"})
@MapperScan("com.gh.mp.mapper")
@EnableSwagger2
@EnableDiscoveryClient
@EnableFeignClients
public class ServiceAclApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceAclApplication.class,args);
    }
}
