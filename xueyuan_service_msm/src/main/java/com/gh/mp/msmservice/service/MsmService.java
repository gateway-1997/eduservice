package com.gh.mp.msmservice.service;

import java.util.Map;

public interface MsmService {
    boolean sendCode(Map<String, Object> map, String phone);
}
