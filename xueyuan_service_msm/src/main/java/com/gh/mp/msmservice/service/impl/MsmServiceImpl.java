package com.gh.mp.msmservice.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.gh.mp.msmservice.service.MsmService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * @Author Eric
 * @Date 2021/5/13 23:46
 * @Version 1.0
 */
@Service
public class MsmServiceImpl implements MsmService {

    public boolean sendCode(Map<String, Object> map, String phone) {

        if (StringUtils.isEmpty(phone)) {
            return false;
        }
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAIsOB7XkCHTGX8", "ASy5lzMwr5KIVEUY3eDhFFijD1RkPC");
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        // 发给谁  ----
        request.putQueryParameter("PhoneNumbers", phone);
        // 签名名称
        request.putQueryParameter("SignName", "薄荷园项目");
        // 模板名称
        request.putQueryParameter("TemplateCode", "SMS_171857472");
        // 验证码 ----
        //request.putQueryParameter("TemplateParam", "{\"code\":\"" + map.get("code") + "\"}");
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(map).toString());
        try {
            CommonResponse response = client.getCommonResponse(request);
            boolean success = response.getHttpResponse().isSuccess();
            return success;
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return false;
    }
}
