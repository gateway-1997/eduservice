package com.gh.mp.msmservice.util;

import java.util.Random;

public class CodeUtil {


    public static String getCode() {
        String content = "qwertyuiopasdfghjklzxcvbnm1234567890ZXCVBNMQWERTYUIOPASDFGHJKL";
        Random random = new Random();
        String result = "";
        for (int i = 0; i < 4; i++) {
            int index = random.nextInt(content.length());
            char c = content.charAt(index);
            result += c;
        }
        return result;
    }

}
