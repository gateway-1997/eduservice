package com.gh.mp.msmservice.controller;

import com.gh.mp.edu.common.R;
import com.gh.mp.edu.utils.JwtUtils;
import com.gh.mp.msmservice.service.MsmService;
import com.gh.mp.msmservice.util.CodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author Eric
 * @Date 2021/5/13 23:45
 * @Version 1.0
 */
@RestController
@RequestMapping("edumsm/msm")
@CrossOrigin
public class MsmController {

    @Autowired
    private MsmService msmService;

    @Autowired
    private RedisTemplate<String, String> template;

    @GetMapping("send/{phone}")
    public R sendMsmCode(@PathVariable String phone) {
        String code = template.opsForValue().get(phone);
        if (!StringUtils.isEmpty(code)) {
            return R.ok().data("msg","请五分钟后再试");
        }
        code = CodeUtil.getCode();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("code", code);
        boolean isSend = msmService.sendCode(map, phone);
        if (isSend) {
            template.boundValueOps(phone).set(code, 5, TimeUnit.MINUTES);
        }
        return R.ok();
    }

}
