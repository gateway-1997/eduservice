package com.gh.mp.xueyuan_service_cms.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gh.mp.edu.common.R;
import com.gh.mp.xueyuan_service_cms.entity.CrmBanner;
import com.gh.mp.xueyuan_service_cms.service.CrmBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author Eric
 * @since 2021-05-11
 */
@RestController
@RequestMapping("/educms/bannerAdmin")
@CrossOrigin
public class BannerAdminController {

    @Autowired
    private CrmBannerService bannerService;

    @GetMapping("pageBanner/{page}/{limit}")
    public R findBannerByPage(@PathVariable Long page,
                              @PathVariable Long limit){
        Page<CrmBanner> pageBanner=new Page<CrmBanner>(page,limit);
        bannerService.findBannerByPage(pageBanner);
        List<CrmBanner> records = pageBanner.getRecords();
        return R.ok().data("items",records).data("total",pageBanner.getTotal());
    }

    @PostMapping("addBanner")
    public R addBanner(@RequestBody CrmBanner crmBanner){
        bannerService.save(crmBanner);
        return R.ok();
    }

    @GetMapping("get/{id}")
    public R findById(@PathVariable String id){
        bannerService.getById(id);
        return R.ok();
    }

    @PutMapping("updateBanner")
    public R updateBanner(@RequestBody CrmBanner crmBanner){
        bannerService.updateById(crmBanner);
        return R.ok();
    }

    @DeleteMapping("removeBanner/{id}")
    public R removeBanner(@PathVariable String id){
        bannerService.removeById(id);
        return R.ok();
    }
}

