package com.gh.mp.xueyuan_service_cms.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gh.mp.xueyuan_service_cms.entity.CrmBanner;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务类
 * </p>
 *
 * @author Eric
 * @since 2021-05-11
 */
public interface CrmBannerService extends IService<CrmBanner> {

    void findBannerByPage(Page<CrmBanner> pageBanner);

    List<CrmBanner> findAllBanner();
}
