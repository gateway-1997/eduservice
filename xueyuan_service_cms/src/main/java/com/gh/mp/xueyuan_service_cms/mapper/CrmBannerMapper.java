package com.gh.mp.xueyuan_service_cms.mapper;

import com.gh.mp.xueyuan_service_cms.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author Eric
 * @since 2021-05-11
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
