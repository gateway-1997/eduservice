package com.gh.mp.xueyuan_service_cms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gh.mp.xueyuan_service_cms.entity.CrmBanner;
import com.gh.mp.xueyuan_service_cms.mapper.CrmBannerMapper;
import com.gh.mp.xueyuan_service_cms.service.CrmBannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务实现类
 * </p>
 *
 * @author Eric
 * @since 2021-05-11
 */
@Service
public class CrmBannerServiceImpl extends ServiceImpl<CrmBannerMapper, CrmBanner> implements CrmBannerService {

    public void findBannerByPage(Page<CrmBanner> pageBanner) {

        baseMapper.selectPage(pageBanner,null);
    }

    @Cacheable(key = "'selectIndexList'",value = "banner")
    public List<CrmBanner> findAllBanner() {
        QueryWrapper<CrmBanner> wrapper=new QueryWrapper<>();
        wrapper.orderByDesc("id");
        wrapper.last("limit 2");
        List<CrmBanner> list = baseMapper.selectList(wrapper);
        return list;
    }
}
