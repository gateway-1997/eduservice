package com.gh.mp.xueyuan_service_cms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@SpringBootApplication
@MapperScan("com.gh.mp.xueyuan_service_cms.mapper")
@ComponentScan("com.gh")
@EnableDiscoveryClient
public class XueyuanServiceCmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(XueyuanServiceCmsApplication.class, args);
    }

}
