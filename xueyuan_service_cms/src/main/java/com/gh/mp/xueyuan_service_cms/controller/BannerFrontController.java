package com.gh.mp.xueyuan_service_cms.controller;

import com.gh.mp.edu.common.R;
import com.gh.mp.xueyuan_service_cms.entity.CrmBanner;
import com.gh.mp.xueyuan_service_cms.service.CrmBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author Eric
 * @Date 2021/5/11 21:40
 * @Version 1.0
 */
@RestController
@RequestMapping("/educms/bannerFront")
@CrossOrigin
public class BannerFrontController {

    @Autowired
    private CrmBannerService bannerService;

    @GetMapping("getAllBanner")
    public R findAllBanner(){
        List<CrmBanner> banners=bannerService.findAllBanner();
        return R.ok().data("items",banners);
    }
}
